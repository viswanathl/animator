package exp.droid.vis.animator;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;

public class AnimatorActivity extends Activity {

	private ImageView iv;

	private int deviceHeight, deviceWidth;

	private int originX, originY;

	private int X,Y;

	private double fromAngle = 90;

	int viewWidth,  viewHeight;

	private Handler handler;

	private Runnable r;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_animator);
		iv = (ImageView) findViewById(R.id.animating_view);
		setDisplayDimension();
		iv.setOnClickListener(mClickListener);
	}

	private OnClickListener mClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			rotateAnimation();
		}
	};

	@Override
	protected void onResume() {
		super.onResume();
	}

	private void rotateAnimation() {
		viewWidth = iv.getWidth();
		viewHeight = iv.getHeight();

		originX = deviceWidth/2 - (viewWidth/2);
		originY = deviceHeight/2 - (viewHeight/2);
		Log.d("", "=============>>" + X + "-" + Y);
		final int toAngle = -105;
		final int radius = originX - 10;
		handler = new Handler();
		r = new Runnable() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if(fromAngle >= toAngle) {
							X = (int)(originX + (Math.sin(fromAngle * (Math.PI/180)) * radius));
							Y = (int)(originY + (Math.cos(fromAngle * (Math.PI/180)) * radius));
							iv.setX(X);
							iv.setY(Y);
							fromAngle -= 2.5;
							handler.postDelayed(r, 2);
						} else {
							fromAngle = 90;
							Log.d("", "=============>>final" + X + "-" + Y);
						}
					}
				});
			}
		};
		
		handler.postDelayed(r, 2);
	}

	private void setDisplayDimension() {
		WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		deviceWidth = metrics.widthPixels;
		deviceHeight = metrics.heightPixels;
	}

}
